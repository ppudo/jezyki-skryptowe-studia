Autor: Paweł Pudo
e-mail: ppudo@student.agh.edu.pl

//***************************************************************************************************************************************************

OPIS SKRYPTÓW

	Skrypty podzielone są na 2 działa zgodnie z dokumentami pdf jakie były na stronie.
	Pliki niewymienione tutaj są etapami testowymi albo innymi wersjami bo chciałem coś sprawdzić.
	
	
//***************************************************************************************************************************************************

PYTHON - ZADANIA 1

	1.	hello_world.py				-----
	2.	get_name.py					-----
	3.	count_file.py				Z uwagi, że pisałem to na windowsie skanuje on folder C:\\Widndows
	4.	code_lock.py				-----
	5.	file_tree_2.py				Ścieżkę należy zmienić w pliku
	6.	jpg_to_png.py				Ścieżkę do obrazka należy podać jako argument
	7.	q_eqn.py					Współczynniki a, b i c podajemy jako argumenty
	8.  sort.py						Po wykonaniu wyświetla tabelę, jeżeli zmienimy znak w podanym miejscu to zmienimy kierunek sortowania
	9.	text_del.py					Jak argument podać lokalizację pliku
	10. text_swap.py				j.w.
	11.	vector_scalar.py			Wektory edytujemy w programie
	12. matrix_add.py				-----
	13. matrix_mux.py				-----
	14.	matrix_det2.py				Przy dużych macierzach trochę to zajmuje ( 10x10 - ok. 90sek )
	
	
//***************************************************************************************************************************************************

PYTHON - ZADANIA 2

	1.	in_out.py					Jako argument podajemy nazwę pliku wyjściowego, a argumenty to lokalizacje pliku do analizy (może być wiele)
	2.	complex_number.py			------
	3.	xml_dom.py					Działa z nbp_a_api.xml - wyświetla tabelę kursów
		xml_sax.py					j.w.
	4.	hist.py						Na końcu wypisuje statystykę oraz całą listę elementów - parametry można zmienić w programie za pomocą zmiennych
	5.	filozofy.py					Powinno działać dobrze, wyświetla status każdego filozofa 
	
	Komentarz dla xml:	Oba programy wymagają podania jako argumentu wejściowego lokalizacji pliku, który mają analizować - nie sprawdzają czy jest to xml
						Najnowszą tabelę walut można pobrać z: http://api.nbp.pl/api/exchangerates/tables/A/