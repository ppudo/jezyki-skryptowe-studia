# J�zyki skryptowe - studia

Zadania wykonane w ramach laboratorium z przedmiotu "Programowanie w j�zykach skryptowych - Python i Bash"

Autor: Pawe� Pudo
e-mail: ppudo@student.agh.edu.pl

//***************************************************************************************************************************************************

OPIS SKRYPT�W

	Skrypty podzielone s� na 2 dzia�a zgodnie z dokumentami pdf jakie by�y na stronie.
	Pliki niewymienione tutaj s� etapami testowymi albo innymi wersjami bo chcia�em co� sprawdzi�.
	
	
//***************************************************************************************************************************************************

PYTHON - ZADANIA 1

	1.	hello_world.py				-----
	2.	get_name.py					-----
	3.	count_file.py				Z uwagi, �e pisa�em to na windowsie skanuje on folder C:\\Widndows
	4.	code_lock.py				-----
	5.	file_tree_2.py				�cie�k� nale�y zmieni� w pliku
	6.	jpg_to_png.py				�cie�k� do obrazka nale�y poda� jako argument
	7.	q_eqn.py					Wsp�czynniki a, b i c podajemy jako argumenty
	8.  sort.py						Po wykonaniu wy�wietla tabel�, je�eli zmienimy znak w podanym miejscu to zmienimy kierunek sortowania
	9.	text_del.py					Jak argument poda� lokalizacj� pliku
	10. text_swap.py				j.w.
	11.	vector_scalar.py			Wektory edytujemy w programie
	12. matrix_add.py				-----
	13. matrix_mux.py				-----
	14.	matrix_det2.py				Przy du�ych macierzach troch� to zajmuje ( 10x10 - ok. 90sek )
	
	
//***************************************************************************************************************************************************

PYTHON - ZADANIA 2

	1.	in_out.py					Jako argument podajemy nazw� pliku wyj�ciowego, a argumenty to lokalizacje pliku do analizy (mo�e by� wiele)
	2.	complex_number.py			------
	3.	xml_dom.py					Dzia�a z nbp_a_api.xml - wy�wietla tabel� kurs�w
		xml_sax.py					j.w.
	4.	hist.py						Na ko�cu wypisuje statystyk� oraz ca�� list� element�w - parametry mo�na zmieni� w programie za pomoc� zmiennych
	5.	filozofy.py					Powinno dzia�a� dobrze, wy�wietla status ka�dego filozofa 
	
	Komentarz dla xml:	Oba programy wymagaj� podania jako argumentu wej�ciowego lokalizacji pliku, kt�ry maj� analizowa� - nie sprawdzaj� czy jest to xml
						Najnowsz� tabel� walut mo�na pobra� z: http://api.nbp.pl/api/exchangerates/tables/A/