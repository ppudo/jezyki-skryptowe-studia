#! python

import sys
import math
import random

#funkcja obliczająca wyznacznik macierzy
def matrix_det( a, poziom = 1 ):		
	if len(a) == len(a[0]):
		if len(a) == 1:
			print( a[0][0] )
			return a[0][0]
		elif len(a) == 2:
			tym1 = a[0][0]*a[1][1] - a[0][1]*a[1][0]
			return tym1
		else:
			detA = pow( -1, 1+poziom ) * a[0][0]
			b = []
			for i in range( len(a)-1 ):
				b.append( [] )
				for j in range( len(a[0])-1 ):
					b[i].append( a[i+1][j+1] )
			
			detA = detA * matrix_det( b, poziom + 1 )
			return detA
	else:
		sys.stdout.write( "Macierz musi być kwadratowa" )

#**************************************************************************************************
matrix = []
rozmiar = 3

random.seed()
for i in range( rozmiar ):
	matrix.append( [] )
	for j in range( rozmiar ):
		matrix[i].append( int( (random.random()-0.5) * 100 ) )
		
matrixDet = matrix_det( matrix )

text = ""
podzial = int( len(matrix) / 2 )
for i in range( len(matrix) ):
	for j in range( len(matrix[0]) ):
		text = text + " %3d" % matrix[i][j]
		
	if i == podzial:
		text = text + "  = %d" % matrixDet
	
	text = text + "\n\r"

sys.stdout.write( text )

		
#Zadanie czternaste
#	Napisz skrypt wyliczaj¡cy wyznacznik macierzy wygenerowanej losowo