#! python

import sys
import random

a = []
b = []
rozmiar = 8

random.seed()
for i in range( rozmiar ):
	a.append( [] )
	b.append( [] )
	for j in range( rozmiar ):
		a[i].append( int( (random.random()-0.5) * 100 ) )
		b[i].append( int( (random.random()-0.5) * 100 ) )
		
y = []
for i in range( len(a) ):
	y.append( [] )
	for j in range( len(b[0]) ):
		y[i].append( 0 )
		for k in range( len(a[0]) ):
			y[i][j] = y[i][j] + a[i][k]*b[k][j]
		
		
text = ""
podzial = int( len(y) / 2 )
for i in range( len(y) ):
	for j in range( len(a[0]) ):
		text = text + " %3d" % a[i][j]
		
	if i == podzial:
		text = text + "  * "
	else:
		text = text + "    "
	
	for j in range( len(b[0]) ):
		text = text + " %3d" % b[i][j]
		
	if i == podzial:
		text = text + "  = "
	else:
		text = text + "    "
		
	for j in range( len(y[0]) ):
		text = text + " %7d" % y[i][j]
	
	text = text + "\n\r"

sys.stdout.write( text )

#Zadanie trzynaste
#	Napisz skrypt realizuj¡cy mno»enie dwóch macierzy o rozmiarach 8x8