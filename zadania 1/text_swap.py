#! python

import sys

if len( sys.argv ) > 1:
	f_in = open( sys.argv[1], 'r' )
	text = f_in.read()
	print( text )
	text = text.replace( " i ", " oraz" )
	text = text.replace( " oraz", " i " )
	text = text.replace( " nigdy", " prawie nigdy" )
	text = text.replace( " dlaczego", " czemu" )
	text = text.replace( "  ", " " )
	print( "\r\n" + text )
	if len( sys.argv ) > 2:
		f_out = open( sys.argv[2], 'w' )
		f_out.write( text )
		f_out.close()
	else:
		nowaNazwa = sys.argv[1].split( '.' )
		nowaNazwa[0] = nowaNazwa[0] + "_edit"
		nowaNazwa2 = nowaNazwa[0] + "." + nowaNazwa[1]
		f_out = open( nowaNazwa2, 'w' )
		f_out.write( text )
		f_out.close()
	f_in.close()
else:
	sys.stdout.write( "Brak podanego pliku\n\r" )

#Zadanie dziesiąte
#	Napisz skrypt zmieniaj¡cy w podanym ci¡gu wej±ciowym (wybierz kilka
#	plików z repozytorium: Tekstowego) nast¦puj¡ce sªowa : i, oraz, nigdy,
#	dlaczego na nast¦puj¡cy zestaw sªów : oraz, i, prawie nigdy, czemu. Zalecan
#	¡ struktur¡ jest sªownik.