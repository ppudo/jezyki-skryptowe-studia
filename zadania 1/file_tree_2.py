#! python
import os
import sys

#Funkcja rysująca drzewo plików podanego polderu
#poziom oznacza kolejny podfoler, w który wchodzimy 
def DrzewoFolderu( path, poziom ):
	for roots, dirs, files in os.walk( path ):
		glebia = poziom * "    "
			
		for dir in dirs:
			sys.stdout.write( glebia + "- " + dir + "\n\r" )
			path2 = path + "/" + dir
			poziom2 = poziom + 1
			DrzewoFolderu( path2, poziom2 )
			
		for file in files:
			sys.stdout.write( glebia + ">" + file + "\n\r")
			
		break;
	
	return;

myPath = "/Users/Paweł/Documents/python/Lab1"
#myPath = "/Users/Paweł"
print( "Drzewo plików: ", myPath, "\n\r" )	
DrzewoFolderu( myPath, 0 )


#Zadanie piąte
#	Napisz rekurencyjne przej±cie drzewa katalogów i wypisanie plików, które
#	znajduj¡ si¦ w eksplorowanej strukturze