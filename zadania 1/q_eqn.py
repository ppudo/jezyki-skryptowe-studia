#! python

import sys
import math

if len( sys.argv ) > 3:
	a = float( sys.argv[1] )
	b = float( sys.argv[2] )
	c = float( sys.argv[3] )
	delta = b*b - 4*a*c
	if delta < 0:
		sys.stdout.write( "Brak pierwiastków rzeczywistych\n\r" )
	elif delta == 0:
		x0 = -b/(2*a)
		sys.stdout.write( "x0 = %.3f" % x0 + "\n\r" )
	else:
		x1 = (-b + math.sqrt(delta))/(2*a)
		x2 = (-b - math.sqrt(delta))/(2*a)
		sys.stdout.write( "x1 = %.3f" % x1 + "\n\r" )
		sys.stdout.write( "x2 = %.3f" % x2 + "\n\r" )
else:
	sys.stdout.write( "Za mało argumentów\n\r" )
	
#Zadanie siódme
#	Napisz skrypt obliczaj¡cy pierwiastki równania kwadratowego w postaci :
#	y = ax2 + bx + c. Wej±ciem skryptu s¡ warto±ci: a, b, c

