#! python

import sys
import math
import random

#funkcja obliczająca wyznacznik macierzy
def matrix_det( a ):		
	if len(a) == len(a[0]):
		if len(a) == 1:
			return a[0][0]
		else:
			detA = 0
			for i in range( len(a) ):
				detB = pow( -1, 2+i ) * a[0][i]
				b = []
				for j in range( len(a)-1 ):
					b.append( [] )
					for k in range( len(a[0]) ):
						if k != i:
							b[j].append( a[j+1][k] )
				
				detA = detA + detB * matrix_det( b )
			return detA
	else:
		sys.stdout.write( "Macierz musi być kwadratowa" )

#**************************************************************************************************
matrix = []
rozmiar = 10

random.seed()
for i in range( rozmiar ):
	matrix.append( [] )
	for j in range( rozmiar ):
		matrix[i].append( int( (random.random()-0.5) * 100 ) )
		
matrixDet = matrix_det( matrix )

text = ""
podzial = int( len(matrix) / 2 )
for i in range( len(matrix) ):
	for j in range( len(matrix[0]) ):
		text = text + " %3d" % matrix[i][j]
		
	if i == podzial:
		text = text + "  = %d" % matrixDet
	
	text = text + "\n\r"

sys.stdout.write( text )

		
#Zadanie czternaste
#	Napisz skrypt wyliczaj¡cy wyznacznik macierzy wygenerowanej losowo