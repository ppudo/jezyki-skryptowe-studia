#! python

import serial
import serial.tools.list_ports

comPort = serial.Serial( "COM7", 9600 )

comPort.write( b'Hello world' )
comPort.close()

comList = serial.tools.list_ports.comports( include_links=False )
comList.sort();

for comName in comList:
	print( comName.device )