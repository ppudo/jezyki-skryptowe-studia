#! python

import threading
import sys
import time
import random

def filozof_jedzenie( numer, widelece ):
	for i in range( 5 ):
		sys.stdout.write( "Filozof %d zaczyna jedzenie \n\r" % numer )
		
		zjadlem = 0
		
		while zjadlem == 0:
			if widelce[numer%5] == 0:
				widelce[numer%5] = 1
				time.sleep( 2 )
				if widelce[(numer+1)%5] == 0:
					widelce[(numer+1)%5] = 1
					sys.stdout.write( "Filozof %d teraz je \n\r" % numer )
					time.sleep( 10 )
					zjadlem = 1
					
					widelce[numer%5] = 0
					widelce[(numer+1)%5] = 0
				else:
					widelce[numer%5] = 0
					sys.stdout.write( "Filozof %d teraz czeka \n\r" % numer )
					time.sleep( int( random.random() * 5 ) )			
		
	sys.stdout.write( "Koniec %d\n\r" % numer )
	
#------------------------------------------------	
widelce = []
for i in range(5):
    widelce.append( 0 )

p1 = threading.Thread( target=filozof_jedzenie, args=(0, widelce) )
p2 = threading.Thread( target=filozof_jedzenie, args=(1, widelce) )
p3 = threading.Thread( target=filozof_jedzenie, args=(2, widelce) )
p4 = threading.Thread( target=filozof_jedzenie, args=(3, widelce) )
p5 = threading.Thread( target=filozof_jedzenie, args=(4, widelce) )

p1.start()
p2.start()
p3.start()
p4.start()
p5.start()

p1.join()
p2.join()
p3.join()
p4.join()
p5.join()

sys.stdout.write( "\n\rFilozofy skończyły jeść\n\r" )

