#! python

import sys

if( len(sys.argv) < 3 ):
	sys.stdout.write( "Brak podengo pliku\n\r" )
else:
	f_out = open( sys.argv[1], 'w' )

	for i in range( len(sys.argv)-2 ):
		f_in = open( sys.argv[2+i], 'r' )
		text = f_in.read()
		
		f_out.write( "Dane z pliku: %s" % sys.argv[2+i] )
		f_out.write( "Ilosc danych: %d\n\r" % len( text ) )
		f_out.write( text )
		f_out.write( "\n\r\n\r" )
		
		f_in.close()
		
	f_out.close()
	sys.stdout.write( "Koniec" )
	
