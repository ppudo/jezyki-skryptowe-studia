#!python

import threading
import sys
import random

def calc_hist( list, array ):
	for a in array:
		list[a] = list[a] + 1

		
#------------------------------------------------
#Konfiguracja
zakres = 5
ilosc = 10000
	
#------------------------------------------------
#Działanie
list = []
for i in range( zakres ):
	list.append(0)

array = []
for i in range( ilosc ):
	array.append( int( random.random() * zakres ) )
	

p1 = threading.Thread( target=calc_hist, args=(list, array[0:int(ilosc/2)]) )
p2 = threading.Thread( target=calc_hist, args=(list, array[int(ilosc/2):ilosc]) )

p1.start()
p2.start()

p1.join()
p2.join()

for i in range(zakres):
	sys.stdout.write( "%d =\t %d\n\r" % (i, list[i]) )
	
sys.stdout.write( "\n\r\r\n" )

for i in range(ilosc ):
	sys.stdout.write( "%d\n\r" % array[i] )