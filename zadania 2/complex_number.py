#! python

import sys
import math

class ComplexNumber:
	re = 0;
	im = 0;
		
	def __init__( self, real = 0, imag = 0 ):
		self.re = real
		self.im = imag
		
	def Add( self, x ):
		if type( x ) == type( self ):
			y = ComplexNumber()
			y.re = self.re + x.re
			y.im = self.im + x.im
			return y
		else:
			raise ValueError( "x is not a ComplexNumber" )
			
	def Subtract( self, x ):
		if type( x ) == type( self ):
			y = ComplexNumber()
			y.re = self.re - x.re
			y.im = self.im - x.im
			return y
		else:
			raise ValueError( "x is not a ComplexNumber" )
	
	def Abs( self ):
		y = self.re*self.re + self.im*self.im
		y = math.sqrt( y )
		return y
	
	def Mux( self, x ):
		if type( x ) == type( self ):
			y = ComplexNumber()
			y.re = self.re*x.re - self.im*x.im
			y.im = self.re*x.im + self.im*x.re
			return y
		else:
			raise ValueError( "x is not a ComplexNumber" )
	
	def ToString( self ):
		if self.im < 0:
			text = "%f - %fi" % ( self.re, self.im*(-1) )
			return text
		else:
			text = "%f + %fi" % ( self.re, self.im )
			return text
	
#----------------------------------------------------------------------------------------------------------------------------------------------------
#Przykład działania	
a = ComplexNumber( 1, 3 )
b = ComplexNumber( 3, -5 )

sys.stdout.write( "a = " + a.ToString() + "\n\r" )
sys.stdout.write( "b = " + b.ToString() + "\n\r" )
sys.stdout.write( "\n\r" )

z1 = a.Add( b )
sys.stdout.write( "a + b = " + z1.ToString() + "\n\r" )

z2 = b.Abs()
sys.stdout.write( "|b| = " )
sys.stdout.write( str(z2) ) 
sys.stdout.write( "\n\r" )

z3 = b.Subtract( a )
sys.stdout.write( "b - a = " + z3.ToString() + "\n\r" )

z4 = a.Mux( b )
sys.stdout.write( "a * b = " + z4.ToString() + "\n\r" )
			