#! python

import xml.sax
import sys

class Rate:
	name = ""
	code = ""
	mid = 0.0
	
	def __init__(self):
		name = ""
		code = ""
		mid = 0.0
		
#--------------------------------------------------------------------------------------------------
#Dla parsera SAX
class RatesHandler( xml.sax.ContentHandler ):
	rates = [];
	
	def __init__(self):
		self.CurrentData = ""
		self.CurrentRate = None
		self.rates = []
		
	def startElement( self, tag, attributes ):
		self.CurrentData = tag
		if self.CurrentData == "Rate":
			self.CurrentRate = Rate()
		
	def endElement ( self, tag ):
		if self.CurrentData != "Currency" and self.CurrentData != "Code" and self.CurrentData != "Mid":
			if self.CurrentRate != None:
				self.rates.append( self.CurrentRate )
				self.CurrentRate = None
		self.CurrentData = ""
		
	def characters( self, content ):
		if self.CurrentData == "Currency":
			self.CurrentRate.name = content
		elif self.CurrentData == "Code":
			self.CurrentRate.code = content
		elif self.CurrentData == "Mid":
			self.CurrentRate.mid = float( content )
			
	def GetRatesTable( self ):
		return self.rates
			
#--------------------------------------------------------------------------------------------------
if len( sys.argv ) != 2:
	sys.stdout.write( "Brak pliku wejściowego\n\r" )
else:
	parser = xml.sax.make_parser()
	parser.setFeature(xml.sax.handler.feature_namespaces, 0)

	Handler = RatesHandler()
	parser.setContentHandler( Handler )

	parser.parse( sys.argv[1] )
	ratesTable = Handler.GetRatesTable()

	#wypisanie tabeli walut
	sys.stdout.write( "%-32s|  %3s  |  %s\n\r" % ( "Nazwa waluty", "Kod", "Kurs" ) )
	sys.stdout.write( "------------------------------------\n\r" );
	for rate in ratesTable:
		name = rate.name
		code = rate.code
		mid = rate.mid
		sys.stdout.write( "%-32s|  %3s  |  %.4f\n\r" % (name, code, mid) )
