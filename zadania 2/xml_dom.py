#! python

import sys
import xml.dom.minidom

if len( sys.argv ) != 2:
	sys.stdout.write( "Brak pliku wejściowego\n\r" )
else:
	domTree = xml.dom.minidom.parse( sys.argv[1] )
	nbpRates = domTree.documentElement

	rates = nbpRates.getElementsByTagName( 'Rate' )

	#wypisanie tabeli walut
	sys.stdout.write( "%-32s|  %3s  |  %s\n\r" % ( "Nazwa waluty", "Kod", "Kurs" ) )
	sys.stdout.write( "------------------------------------\n\r" );
	for rate in rates:
		name = rate.getElementsByTagName('Currency')[0].childNodes[0].data
		code = rate.getElementsByTagName('Code')[0].childNodes[0].data
		mid = float( rate.getElementsByTagName('Mid')[0].childNodes[0].data )
		sys.stdout.write( "%-32s|  %3s  |  %.4f\n\r" % (name, code, mid) )
		
		
	#coś tu nie działa jak należy
	midTag = rate.getElementsByTagName('Mid')[0]
	midTag.firstChild.replaceWholeText( 'Kurs' )

	fileName = sys.argv[1].replace( ".xml", "_changed.xml" )
	file = open( fileName, "wb" )
	nbpRates.writexml( file )
	file.close()