Dane z pliku: ..\Lab1\matrix_det_time.py
Ilosc danych: 1414
#! python

import sys
import math
import random
import datetime

#funkcja obliczająca wyznacznik macierzy
def matrix_det( a ):		
	if len(a) == len(a[0]):
		if len(a) == 1:
			return a[0][0]
		else:
			detA = 0
			for i in range( len(a) ):
				detB = pow( -1, 2+i ) * a[0][i]
				b = []
				for j in range( len(a)-1 ):
					b.append( [] )
					for k in range( len(a[0]) ):
						if k != i:
							b[j].append( a[j+1][k] )
				
				detA = detA + detB * matrix_det( b )
			return detA
	else:
		sys.stdout.write( "Macierz musi być kwadratowa" )

#**************************************************************************************************
matrix = []
rozmiar = 10

random.seed()
for i in range( rozmiar ):
	matrix.append( [] )
	for j in range( rozmiar ):
		matrix[i].append( int( (random.random()-0.5) * 100 ) )

czas = datetime.datetime.now()
matrixDet = matrix_det( matrix )
czas = datetime.datetime.now() - czas

text = ""
podzial = int( len(matrix) / 2 )
for i in range( len(matrix) ):
	for j in range( len(matrix[0]) ):
		text = text + " %3d" % matrix[i][j]
		
	if i == podzial:
		text = text + "  = %d" % matrixDet
	
	text = text + "\n\r"

sys.stdout.write( text )
czas_ms = (czas.days * 24 * 60 * 60 + czas.seconds) * 1000 + czas.microseconds / 1000.0
sys.stdout.write( "\n\rCzas wykonywania: %d" % czas_ms )

		
#Zadanie czternaste
#	Napisz skrypt wyliczaj¡cy wyznacznik macierzy wygenerowanej losowo

Dane z pliku: ..\Lab1\matrix_add.py
Ilosc danych: 1023
#! python

import sys
import random

a = []
b = []
rozmiar = 128

random.seed()
for i in range( rozmiar ):
	a.append( [] )
	b.append( [] )
	for j in range( rozmiar ):
		a[i].append( int( random.random() * 1000 ) )
		b[i].append( int( random.random() * 1000 ) )

y = []
for i in range( len(a) ):
	y.append( [] )
	for j in range( len(a[0]) ):
		y[i].append( a[i][j] + b[i][j] )
		
text = ""
podzial = int( len(y) / 2 )
for i in range( len(y) ):
	for j in range( len(a[0]) ):
		text = text + " %3d" % a[i][j]
		
	if i == podzial:
		text = text + "  + "
	else:
		text = text + "    "
	
	for j in range( len(b[0]) ):
		text = text + " %3d" % b[i][j]
		
	if i == podzial:
		text = text + "  = "
	else:
		text = text + "    "
		
	for j in range( len(y[0]) ):
		text = text + " %4d" % y[i][j]
	
	text = text + "\n\r"

sys.stdout.write( text )

#Zadanie dwunaste
#	Napisz skrypt sumuj¡cy dwie macierze o rozmiarach 128x128. Wykorzystaj
#	generator liczb losowych do wygenerowania macierzy. Zrealizuj operacj
#	¦ z wykorzystaniem
	

